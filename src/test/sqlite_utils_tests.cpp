/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "sqlite_utils/binding.h"
#include "sqlite_utils/database.h"
#include "sqlite_utils/prepared_statement.h"
#include "sqlite_utils/result_set.h"
#include "sqlite_utils/transaction.h"
#include "sqlite_utils/types.h"

#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>

#include <iterator>
#include <stdexcept>
#include <vector>

namespace {
struct MyCustomError : std::exception {};
}  // namespace

namespace sqlite_utils::test {

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Simple insert / retrieval") {
  GIVEN("A database with a single table") {
    Database db;

    db.Execute("CREATE TABLE t(i integer);");

    WHEN("inserting a few entries") {
      PreparedStatement statement(db, "INSERT INTO t(i) VALUES(:i);");

      const std::vector values{5, 6, 7, 8};

      std::for_each(values.begin(), values.end(), [&](auto v) {
        Binding bind(statement, {{":i", v}});
        statement.Step();
        statement.Reset();
      });

      THEN("retrieving the rows yields the same result") {
        PreparedStatement select(db, "SELECT i FROM t;");
        ResultSet resultSet(select);

        std::vector<int> retrievedValues;
        std::transform(resultSet.begin(), resultSet.end(),
                       std::back_inserter(retrievedValues),
                       [](const auto& row) {
                         return static_cast<sqlite_utils::Int>(row["i"]);
                       });

        CHECK(retrievedValues == values);
      }
    }
  }
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Conversions") {
  GIVEN("A column with an integer value and a text value row") {
    Database db;
    db.Execute("CREATE TABLE t(val); INSERT INTO t VALUES (1), ('2');");

    WHEN("retrieving the values") {
      PreparedStatement stmt(db, "SELECT val FROM t ORDER BY rowid;");
      ResultSet resultSet(stmt);
      auto results = resultSet.begin();

      THEN("Without type hinting, only valid conversions should succeed") {
        CHECK(1 == static_cast<Int>((*results)["val"]));
        CHECK_THROWS(static_cast<Text>((*results)["val"]));
        ++results;
        CHECK(std::string("2") == static_cast<Text>((*results)["val"]));
        CHECK_THROWS(static_cast<Int>((*results)["val"]));
      }

      THEN("With type hinting, all conversions should succeed") {
        CHECK(1 == (*results)["val"].Integer());
        CHECK(std::string("1") == (*results)["val"].String());
        ++results;
        CHECK(std::string("2") == (*results)["val"].String());
        CHECK(2 == (*results)["val"].Integer());
      }
    }
  }
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Transactions") {
  GIVEN("Two connections and a transaction") {
    Database db("file::memory:?cache=shared");
    Database db2("file::memory:?cache=shared");

    {
      Transaction t(db);
      db.Execute("CREATE TABLE t(val);");

      THEN("No dirty reads should occur") {
        CHECK_THROWS(PreparedStatement(db2, "SELECT * FROM t;"));
      }
    }

    THEN("Committed read should be successful") {
      CHECK_NOTHROW(PreparedStatement(db2, "SELECT * FROM t;"));
    }
  }

  GIVEN("A rolled back transaction") {
    Database db;
    try {
      Transaction t(db);
      db.Execute("CREATE TABLE t(val);");
      throw MyCustomError{};
    } catch (const MyCustomError&) {
    }

    THEN("No effects should be visible") {
      CHECK_THROWS(PreparedStatement(db, "SELECT * FROM t;"));
    }
  }
}

namespace {
void RecursiveTransaction(Database& db, const unsigned counter) {
  if (counter == 0) {
    throw MyCustomError{};
  }

  Transaction t(db);
  RecursiveTransaction(db, counter - 1);
}
}  // namespace

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Nested Transactions") {
  Database db("file::memory:?cache=shared");
  Database db2("file::memory:?cache=shared");

  {
    Transaction t1(db);
    db.Execute("CREATE TABLE t(val);");
    try {
      Transaction t2(db);
      db.Execute("CREATE TABLE t2(val);");
      throw MyCustomError{};
    } catch (const MyCustomError&) {
    }
  }

  CHECK_NOTHROW(PreparedStatement(db2, "SELECT * from t;"));
  CHECK_THROWS(PreparedStatement(db2, "SELECT * from t2;"));

  try {
    RecursiveTransaction(db, 127);
  } catch (const MyCustomError&) {
  }
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Statistics") {
  GIVEN("A simple database and one prepared statement") {
    Database db;

    db.Execute("CREATE TABLE t1(val1 int);");
    db.Execute("INSERT INTO t1(val1) VALUES (1), (2), (3);");

    PreparedStatement stmt{db, "SELECT * FROM t1;"};

    THEN("Initially the counters should be zero") {
      CHECK(stmt.StatsVmSteps() == 0);
      CHECK(stmt.StatsExecutions() == 0);
      CHECK_THAT(stmt.StatsAverageVmSteps(), Catch::WithinULP(0., 0));
    }

    WHEN("Querying the statement once") {
      {
        ResultSet rs{stmt};
        CHECK(std::distance(rs.begin(), rs.end()) == 3);

        THEN("The counters should reflect this") {
          CHECK_THAT(stmt.StatsVmSteps(), Catch::WithinRel(15, 0.01));
          CHECK(stmt.StatsExecutions() == 1);
          CHECK_THAT(stmt.StatsAverageVmSteps(), Catch::WithinRel(15, 0.01));
        }
      }

      WHEN("Querying a second time") {
        ResultSet rs{stmt};
        CHECK(std::distance(rs.begin(), rs.end()) == 3);

        THEN("The counters should increase as well") {
          CHECK_THAT(stmt.StatsVmSteps(), Catch::WithinRel(30, 0.01));
          CHECK(stmt.StatsExecutions() == 2);
          CHECK_THAT(stmt.StatsAverageVmSteps(), Catch::WithinRel(15, 0.01));
        }
      }
    }
  }
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Copyable iterator") {
  GIVEN("A simple database and one prepared statement") {
    Database db;

    db.Execute("CREATE TABLE t(i integer);");
    db.Execute("INSERT INTO t(i) VALUES (1), (2), (3);");

    PreparedStatement stmt{db, "SELECT * FROM t;"};

    ResultSet rs{stmt};

    // Temporary iterator that is destructed on expression end and copied
    auto it = ++rs.begin();

    CHECK(static_cast<Int>((*it)["i"]) == 2);
  }
}
}  // namespace sqlite_utils::test
