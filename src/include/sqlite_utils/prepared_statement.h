/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "sqlite_utils/database.h"  // for Database

#include <sqlite3.h>  // for sqlite3_finalize, sqlite3_stmt, sqlite3_bind_parameter_index, sqlite3_cle...

#include <algorithm>  // for find
#include <cassert>    // for assert
#include <cstddef>    // for size_t
#include <iterator>   // for distance
#include <memory>     // for allocator, unique_ptr, operator==
#include <stdexcept>  // for runtime_error
#include <string>     // for basic_string, operator+, operator==
#include <vector>     // for vector

namespace sqlite_utils {

namespace detail {
inline sqlite3_stmt* PrepareStatement(sqlite3* db, const char* const query) {
  assert(db != nullptr);
  const char* dummy = nullptr;
  sqlite3_stmt* stmt{};
  if (sqlite3_prepare_v2(db, query, -1, &stmt, &dummy) != SQLITE_OK) {
    throw std::runtime_error(std::string("Failed to prepare statement: ") +
                             query);
  }
  assert(stmt != nullptr);
  return stmt;
}
}  // namespace detail

enum class SqliteType { Integer, Float, Blob, Null, Text };

class PreparedStatement final {
 public:
  PreparedStatement(Database& db, const std::string& query)
      : statement_(detail::PrepareStatement(*db, query.c_str()),
                   &sqlite3_finalize) {
    PrepareColumnInfos();
  }

  // TODO remove escape hatch
  sqlite3_stmt* Get() { return statement_.get(); }
  sqlite3_stmt* operator*() { return Get(); }

  // TODO check return value
  void Reset() { sqlite3_reset(statement_.get()); }

  void ClearBindings() { sqlite3_clear_bindings(statement_.get()); }

  int Step() { return sqlite3_step(statement_.get()); }

  bool IsBusy() const { return sqlite3_stmt_busy(statement_.get()) != 0; }

  int ParameterIndex(const char* parameterName) const;

  int ColumnIndex(const char* columnName) const;

  SqliteType ColumnType(int index) const;

  int StatsVmSteps() const;

  int StatsExecutions() const;

  double StatsAverageVmSteps() const;

 private:
  void PrepareColumnInfos() const;

  static SqliteType ConvertColumnType(int sqliteColumnType);

  const std::unique_ptr<sqlite3_stmt, decltype(&sqlite3_finalize)> statement_;
  mutable std::vector<std::string> columnNames_;

  friend bool operator==(const PreparedStatement& lhs,
                         const PreparedStatement& rhs);

  static constexpr int kNoReset = 0;
};

inline bool operator==(const PreparedStatement& lhs,
                       const PreparedStatement& rhs) {
  return lhs.statement_ == rhs.statement_;
}

inline int PreparedStatement::ParameterIndex(const char* parameterName) const {
  const auto index =
      sqlite3_bind_parameter_index(statement_.get(), parameterName);
  if (index == 0) {
    throw std::runtime_error(
        std::string("Could not find parameter with name ") + parameterName);
  }
  return index;
}

inline int PreparedStatement::ColumnIndex(const char* columnName) const {
  assert(!columnNames_.empty());

  const auto index =
      std::find(columnNames_.begin(), columnNames_.end(), columnName);
  if (index == columnNames_.end()) {
    throw std::runtime_error(std::string("Could not find '") + columnName +
                             "' amongst columns in result set");
  }

  const auto result = std::distance(columnNames_.begin(), index);
  return static_cast<int>(result);
}

inline SqliteType PreparedStatement::ColumnType(const int index) const {
  return ConvertColumnType(sqlite3_column_type(statement_.get(), index));
}

inline void PreparedStatement::PrepareColumnInfos() const {
  assert(statement_);
  assert(columnNames_.empty());

  const auto numColumns = sqlite3_column_count(statement_.get());
  columnNames_.reserve(static_cast<std::size_t>(numColumns));

  for (int index = 0; index < numColumns; ++index) {
    columnNames_.emplace_back(sqlite3_column_name(statement_.get(), index));
  }
}

inline SqliteType PreparedStatement::ConvertColumnType(int sqliteColumnType) {
  switch (sqliteColumnType) {
    case SQLITE_INTEGER:
      return SqliteType::Integer;
    case SQLITE_FLOAT:
      return SqliteType::Float;
    case SQLITE_BLOB:
      return SqliteType::Blob;
    case SQLITE_NULL:
      return SqliteType::Null;
    case SQLITE_TEXT:
      return SqliteType::Text;
    default:
      assert(false && "Unexpected integer value for sqlite column type");
      return SqliteType::Null;
  }
}

inline int PreparedStatement::StatsVmSteps() const {
  return sqlite3_stmt_status(statement_.get(), SQLITE_STMTSTATUS_VM_STEP,
                             kNoReset);
}

inline int PreparedStatement::StatsExecutions() const {
  return sqlite3_stmt_status(statement_.get(), SQLITE_STMTSTATUS_RUN, kNoReset);
}

inline double PreparedStatement::StatsAverageVmSteps() const {
  const auto executions = StatsExecutions();
  if (executions == 0) {
    return 0.;
  }
  return static_cast<double>(StatsVmSteps()) / StatsExecutions();
}
}  // namespace sqlite_utils
