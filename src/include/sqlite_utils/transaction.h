/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "database.h"

#include <stdexcept>

namespace sqlite_utils {

class Transaction {
 public:
  explicit Transaction(Database& db) : db_{db} {
    db_.Execute("SAVEPOINT RAII;");
  }

  // This might throw and we accept this as we don't expect anyone to use
  // transactions during stack unwinding
  ~Transaction() {
    if (exceptionCounter_ != std::uncaught_exceptions()) {
      db_.Execute("ROLLBACK TRANSACTION TO SAVEPOINT RAII;");
    }

    db_.Execute("RELEASE SAVEPOINT RAII;");
  }

  Transaction(const Transaction&) = delete;
  Transaction(Transaction&&) = delete;
  Transaction& operator=(const Transaction&) = delete;
  Transaction& operator=(Transaction&&) = delete;

 private:
  Database& db_;
  int exceptionCounter_ = std::uncaught_exceptions();
};
}  // namespace sqlite_utils
