/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "sqlite_utils/prepared_statement.h"

#include <sqlite3.h>

#include <initializer_list>  // for initializer_list
#include <iosfwd>            // for string
#include <optional>          // for optional
#include <stdexcept>         // for runtime_error
#include <tuple>             // for get, tuple
#include <variant>           // for variant

namespace sqlite_utils {

class Binding {
  // TODO align these types with types.h
  using SqliteBindArgument =
      std::variant<const std::string, int, double, std::optional<std::string>>;
  using SqliteBindPair = std::tuple<const char*, SqliteBindArgument>;
  using BindInit = std::initializer_list<SqliteBindPair>;

 public:
  Binding(PreparedStatement& statement, const BindInit& bindMap)
      : statement_(statement) {
    Bind(bindMap);
  }

  Binding() = delete;
  Binding(Binding&&) = delete;
  Binding(const Binding&) = delete;
  Binding& operator=(const Binding&) = delete;
  Binding& operator=(Binding&&) = delete;

  ~Binding() { statement_.ClearBindings(); }

 private:
  void Bind(const BindInit& bindMap);
  void BindSingle(int index, const SqliteBindArgument& argument);

  PreparedStatement& statement_;
};

inline void Binding::Bind(const BindInit& bindMap) {
  for (const auto& pair : bindMap) {
    const auto& parameterName = std::get<0>(pair);
    const auto& argument = std::get<1>(pair);
    const auto index = statement_.ParameterIndex(parameterName);

    BindSingle(index, argument);
  }
}

namespace detail {
template <class... Ts>
struct overloaded : Ts... {  // NOLINT(readability-identifier-naming)
  using Ts::operator()...;
};

template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;
}  // namespace detail

inline void Binding::BindSingle(const int index,
                                const SqliteBindArgument& argument) {
  using namespace detail;
  auto* stmt = statement_.Get();

  auto visitor = overloaded{
      [&](const std::string& value) {
        return sqlite3_bind_text(
            stmt, index, value.c_str(), -1,
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-cstyle-cast)
            SQLITE_TRANSIENT);
      },
      [&](const int value) { return sqlite3_bind_int(stmt, index, value); },
      [&](const double value) {
        return sqlite3_bind_double(stmt, index, value);
      }};

  const auto bindResult =
      std::visit(overloaded{[&](const std::optional<std::string>& maybeValue) {
                              if (!maybeValue) {
                                return sqlite3_bind_null(stmt, index);
                              }
                              return visitor(*maybeValue);
                            },
                            [&](const auto& value) { return visitor(value); }},
                 argument);

  if (bindResult != SQLITE_OK) {
    throw std::runtime_error("Failed to bind value");
  }
}
}  // namespace sqlite_utils
