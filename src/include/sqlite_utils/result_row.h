/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "sqlite_utils/prepared_statement.h"
#include "sqlite_utils/types.h"

#include <sqlite3.h>

#include <type_traits>
#include <variant>  // for variant, std::get

namespace sqlite_utils {
using SqliteValue = std::variant<Int, Double, Text>;

namespace detail {

// Source: https://stackoverflow.com/a/45892305/522251
template <typename T, typename variant_t>
struct isVariantMember;

template <typename T, typename... all_t>
struct isVariantMember<T, std::variant<all_t...>>
    : public std::disjunction<std::is_same<T, all_t>...> {};

template <typename T>
struct ValueFetcher;

template <>
struct ValueFetcher<Int> {
  Int operator()(PreparedStatement& statement, const int index) const {
    static_assert(sizeof(Int) == sizeof(sqlite3_int64));
    return static_cast<Int>(sqlite3_column_int64(statement.Get(), index));
  }
};

template <>
struct ValueFetcher<Text> {
  Text operator()(PreparedStatement& statement, const int index) const {
    // TODO handle unicode correctly here!
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    return reinterpret_cast<const char*>(
        sqlite3_column_text(statement.Get(), index));
  }
};

template <>
struct ValueFetcher<Double> {
  Double operator()(PreparedStatement& statement, const int index) const {
    return sqlite3_column_double(statement.Get(), index);
  }
};

template <>
struct ValueFetcher<SqliteValue> {
  SqliteValue operator()(PreparedStatement& statement, const int index) const {
    switch (statement.ColumnType(index)) {
      case SqliteType::Integer:
        return ValueFetcher<Int>{}(statement, index);
      case SqliteType::Text:
        return ValueFetcher<Text>{}(statement, index);
      case SqliteType::Float:
        return ValueFetcher<Double>{}(statement, index);
      case SqliteType::Blob:
      case SqliteType::Null:
        assert(false && "Unimplemented");
        return "INVALID -- UNIMPLEMENTED";
    }

    assert(false && "Unexpected column type");
    return "UNEXPECTED TYPE";
  }
};

struct PendingFetcher {
  PendingFetcher(PreparedStatement& statement, const int index)
      : statement_{statement}, index_{index} {}

  Int Integer() { return ValueFetcher<Int>{}(statement_, index_); }

  Double Float() {
    return ValueFetcher<sqlite_utils::Double>{}(statement_, index_);
  }

  Text String() {
    return ValueFetcher<sqlite_utils::Text>{}(statement_, index_);
  }

  template <typename T, typename = std::enable_if_t<
                            detail::isVariantMember<T, SqliteValue>{}>>
  // NOLINTNEXTLINE(google-explicit-constructor)
  operator T() const {
    return std::get<T>(ValueFetcher<SqliteValue>{}(statement_, index_));
  }

 private:
  PreparedStatement& statement_;
  int index_;
};

// Types are not contained and conversion should not be possible
static_assert(!std::is_convertible_v<PendingFetcher, float>);
static_assert(!std::is_convertible_v<PendingFetcher, char>);
// Types are contained and conversion should be possible
static_assert(std::is_convertible_v<PendingFetcher, Int>);
static_assert(std::is_convertible_v<PendingFetcher, Double>);
static_assert(std::is_convertible_v<PendingFetcher, Text>);
}  // namespace detail

class Row {
 private:
 public:
  explicit Row(PreparedStatement& statement) : statement_{statement} {}

  detail::PendingFetcher operator[](const int index) const {
    return detail::PendingFetcher(statement_, index);
  }

  detail::PendingFetcher operator[](const char* columnName) const {
    const auto index = statement_.ColumnIndex(columnName);
    return detail::PendingFetcher(statement_, index);
  }

 private:
  // TODO create value semantics and allow to copy these things by storing
  // vector<cells> instead?
  // Would also need to store the columnName vector inside...
  PreparedStatement& statement_;
};
}  // namespace sqlite_utils
