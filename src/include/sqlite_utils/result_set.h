/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "sqlite_utils/prepared_statement.h"  // for PreparedStatement, operator==
#include "sqlite_utils/result_row.h"
#include "sqlite_utils/types.h"

#include <sqlite3.h>  // for sqlite3_column_double, sqlite3_column_int64

#include <cassert>    // for assert
#include <cstddef>    // for ptrdiff_t
#include <iterator>   // for input_iterator_tag
#include <stdexcept>  // for runtime_error

namespace sqlite_utils {

class ResultSet {
 private:
  class Iterator;

 public:
  explicit ResultSet(PreparedStatement& statement) : statement_{statement} {}

  ResultSet(const ResultSet&) = delete;
  ResultSet(ResultSet&&) = delete;
  ResultSet& operator=(const ResultSet&) = delete;
  ResultSet& operator=(ResultSet&&) = delete;
  ~ResultSet() { statement_.Reset(); }

  // NOLINTNEXTLINE(readability-identifier-naming)
  Iterator begin();
  // NOLINTNEXTLINE(readability-identifier-naming)
  Iterator end();

  // TODO could add these with value semantics for Row
  //  Row front() { return *begin(); }

  PreparedStatement& statement_;
};

class ResultSet::Iterator {
 public:
  using iterator_category = std::input_iterator_tag;
  using value_type = Row;
  using reference = value_type;
  using difference_type = std::ptrdiff_t;
  using pointer = void;

  Iterator() = default;

  explicit Iterator(PreparedStatement* statement) : statement_{statement} {
    assert(!statement->IsBusy());
    // TODO meh
    increment();
  }

  Iterator(const Iterator&) = default;
  Iterator(Iterator&& other) = default;
  Iterator& operator=(const Iterator&) = default;
  Iterator& operator=(Iterator&&) = default;

  ~Iterator() { Reset(); }

  bool operator==(const Iterator& other) const { return equal(other); }

  bool operator!=(const Iterator& other) const { return !(*this == other); }

  reference operator*() const { return dereference(); }

  Iterator& operator++() {
    increment();
    return *this;
  }

 private:
  void Reset() { statement_ = nullptr; }

  // NOLINTNEXTLINE(readability-identifier-naming)
  void increment() {
    const auto sqliteResult = statement_->Step();
    if (sqliteResult == SQLITE_DONE) {
      Reset();
    } else if (sqliteResult == SQLITE_ROW) {
      // DO NOTHING
    } else {
      // TODO extend error handling? (Or is general error handler enough?)
      throw std::runtime_error("Unexpected sqlite result");
    }
  }

  // TODO fine enough to only support inequality with the end() iterator?
  // NOLINTNEXTLINE(readability-identifier-naming)
  [[nodiscard]] bool equal(const Iterator& other) const {
    return this->statement_ == other.statement_;
  }

  // TODO loads of lifetime issues with keeping the preparedstatement alive
  // while the other ones might outlive it.
  // NOLINTNEXTLINE(readability-identifier-naming)
  [[nodiscard]] Row dereference() const {
    assert(statement_ != nullptr);
    return Row(*statement_);
  }

  PreparedStatement* statement_;
};

// NOLINTNEXTLINE(readability-identifier-naming)
inline ResultSet::Iterator ResultSet::begin() { return Iterator(&statement_); }

// readability-identifier-naming, readability-convert-member-functions-to-static
// NOLINTNEXTLINE
inline ResultSet::Iterator ResultSet::end() { return Iterator{}; }

}  // namespace sqlite_utils
