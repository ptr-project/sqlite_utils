/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "sqlite_utils/database.h"

#include "sqlite_utils/detail/database_trace.h"

#include <sqlite3.h>  // for sqlite3, SQLITE_OK, sqlite3_close, sqlite3_config, sqlite3_open, sqlite3_trace, SQLITE_...

#include <cassert>
#include <stdexcept>  // for runtime_error
#include <type_traits>

namespace sqlite_utils {
namespace detail {

inline bool ConfigureSqlite() {
  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
  return sqlite3_config(SQLITE_CONFIG_LOG, ErrorLogCallback) == SQLITE_OK;
}

inline sqlite3* OpenDatabase(const char* aDbName) {
  static bool configSqlite = ConfigureSqlite();

  if (!configSqlite) {
    throw std::runtime_error("Could not configure sqlite");
  }

  const auto flags =
      SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE | SQLITE_OPEN_URI;  // NOLINT

  sqlite3* db = nullptr;
  if (sqlite3_open_v2(aDbName, &db, flags, nullptr) != SQLITE_OK) {
    throw std::runtime_error("Could not open database");
  }

  // TODO make configurable
  (void)&TraceCallback;
  //  sqlite3_trace(db, TraceCallback, nullptr);
  return db;
}

inline void CloseDatabase(sqlite3* db) noexcept {
  const auto errorCode = sqlite3_close(db);
  if (errorCode != SQLITE_OK) {
    ErrorLogCallback(nullptr, errorCode, "Could not close database!");
  }
}

static_assert(!std::is_copy_constructible<Database>::value);
static_assert(std::is_move_constructible<Database>::value);
static_assert(!std::is_copy_assignable<Database>::value);
static_assert(std::is_move_assignable<Database>::value);
}  // namespace detail

Database::Database(const char* dbName)
    : db_(detail::OpenDatabase(dbName), &detail::CloseDatabase) {}

Database::Database() : Database(":memory:") {}

sqlite3* Database::operator*() const {
  assert(db_);
  return db_.get();
}

// NOLINTNEXTLINE(readability-make-member-function-const)
void Database::Execute(const char* query) {
  const auto res = sqlite3_exec(operator*(), query, nullptr, nullptr, nullptr);
  if (res != SQLITE_OK) {
    throw std::runtime_error("Failed to execute statement");
  }
}

}  // namespace sqlite_utils
