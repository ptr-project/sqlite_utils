/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "sqlite_utils/binding.h"
#include "sqlite_utils/database.h"
#include "sqlite_utils/prepared_statement.h"
#include "sqlite_utils/result_set.h"
#include "sqlite_utils/types.h"

#include <sqlite3.h>

#include <catch2/catch.hpp>
#include <catch2_extended/custom_macros.h>

#include <algorithm>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

namespace sqlite_utils::benchmark {

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("A table with 100'000 entries") {
  Database db;

  db.Execute("CREATE TABLE t(i integer);");

  PreparedStatement statement(db, "INSERT INTO t(i) VALUES(:i);");

  std::vector<int> values(100'000);
  std::iota(values.begin(), values.end(), 1);

  std::for_each(values.begin(), values.end(), [&](auto v) {
    Binding bind(statement, {{":i", v}});
    // TODO add 'step' function that directly takes binding arguments?
    statement.Step();
  });

  BENCHMARK_ADVANCED_STABLE("sqlite3")(auto meter) {
    // Go through the full lifecycle with https://sqlite.org/c3ref/stmt.html
    sqlite3_stmt* stmt = nullptr;
    if (SQLITE_OK !=
        sqlite3_prepare_v2(*db, "SELECT i from t;", -1, &stmt, nullptr)) {
      throw std::runtime_error("Failed to prepare statement");
    }

    int result = 0l;

    meter.measure([&] {
      while (SQLITE_ROW == sqlite3_step(stmt)) {
        result += (sqlite3_column_int(stmt, 0));
      }
    });

    if (SQLITE_OK != sqlite3_reset(stmt)) {
      throw std::runtime_error("Failed to reset the prepared statement");
    }

    if (SQLITE_OK != sqlite3_finalize(stmt)) {
      throw std::runtime_error("Failed to finalize the prepared statement");
    }

    return result;
  };

  BENCHMARK_ADVANCED_STABLE("sqlite_utils, full hinting")(auto meter) {
    PreparedStatement select(db, "SELECT i FROM t;");

    auto result = 0l;

    meter.measure([&] {
      ResultSet resultSet(select);
      for (auto&& row : resultSet) {
        result += row[0].Integer();
      }
    });

    return result;
  };

  BENCHMARK_ADVANCED_STABLE("sqlite_utils, type hinting")(auto meter) {
    PreparedStatement select(db, "SELECT i from t;");

    auto result = 0l;

    meter.measure([&] {
      ResultSet resultSet(select);
      for (auto&& row : resultSet) {
        result += row["i"].Integer();
      }
    });
  };

  BENCHMARK_ADVANCED_STABLE("sqlite_utils, index hinting")(auto meter) {
    PreparedStatement select(db, "SELECT i from t;");

    auto result = 0l;

    meter.measure([&] {
      ResultSet resultSet(select);
      for (auto&& row : resultSet) {
        sqlite_utils::Int i = row[0];
        result += i;
      }
    });
  };

  BENCHMARK_ADVANCED_STABLE("sqlite_utils, no hinting")(auto meter) {
    PreparedStatement select(db, "SELECT i from t;");

    auto result = 0l;

    meter.measure([&] {
      ResultSet resultSet(select);
      for (auto&& row : resultSet) {
        sqlite_utils::Int i = row["i"];
        result += i;
      }
    });
  };
}
}  // namespace sqlite_utils::benchmark
