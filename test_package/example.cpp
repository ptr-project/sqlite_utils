/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <sqlite_utils/binding.h>
#include <sqlite_utils/database.h>
#include <sqlite_utils/prepared_statement.h>
#include <sqlite_utils/result_set.h>
#include <sqlite_utils/transaction.h>
#include <sqlite_utils/types.h>

#include <iostream>

int main() {
  sqlite_utils::Database db;
  db.Execute("CREATE TABLE t(val);");
  db.Execute("INSERT INTO t VALUES (1);");

  sqlite_utils::PreparedStatement stmt(db, "SELECT val FROM t;");
  sqlite_utils::ResultSet rs(stmt);
  for (const auto& row : rs) {
    if (row["val"].Integer() == 1) {
      return 0;
    } else {
      return 1;
    }
  }
}
