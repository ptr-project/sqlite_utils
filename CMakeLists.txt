cmake_minimum_required(VERSION 3.11)
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

set(CMAKE_TOOLCHAIN_FILE "${CMAKE_CURRENT_LIST_DIR}/cmake/toolchains/base.cmake"
  CACHE FILEPATH "Path to the toolchain to use")

project(sqlite_utils)

option(BUILD_TESTING "Build the tests." ON)
if(BUILD_TESTING)
    enable_testing()
endif()

include(ConanBootstrap)

include(${CMAKE_CURRENT_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup(TARGETS)

include(ConfigureWarnings)


add_subdirectory(src)
