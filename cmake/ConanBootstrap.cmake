include_guard(GLOBAL)

if(NOT EXISTS "${CMAKE_CURRENT_BINARY_DIR}/conan-bootstrap.cmake")
  file(DOWNLOAD "https://gitlab.com/ptr-project/conan-config/-/raw/main/cmake/ConanBootstrap.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/conan-bootstrap.cmake"
    EXPECTED_HASH SHA1=c6ccb263fa8121d92c5989cecf2694f9a6ca2faa)
endif()

include(${CMAKE_CURRENT_BINARY_DIR}/conan-bootstrap.cmake)

